const path = require('path')
const os = require('os')

module.exports = {
  storage: path.join(os.homedir(), '.cobox'),
  hubStorage: path.join(os.homedir(), '.cobox-hub'),
  mount: path.join(os.homedir(), 'cobox'),
  keyIds: {
    identity: 0,
    log: 0,
    metadata: 1,
    content: 2
  }
}
